# Creative Realtors' Website #

### Description:

The Creative Realtors' website is a project being undertaken  
by the Kaprisoft Core development team. The goal is to create  
a usable and great looking website for the Creative Realtors,  
a team of two Realtors&reg;.

### Goals: 

The main goal of this project is to create a user-friendly  
and great-looking website for the Creative Realtors, a mother-
daughter team of two Realtors&reg;

Secondary goals include things such as continuous integration
with the Google Cloud Platform and/or Azure (Currently GCP).

### Instructions for Contribution:

Anyone who'd like to contribute to this website's code may
conditionally do so with an agreement between Kaprisoft and
the individual contributor.

Contributions include a two-part interaction:
the first part being Commit to Repository and the second
part being the distribution of code. By contributing to
this repository you automatically agree to the distrobution
of your code through the Creative Realtors' website without
any further consent.

Your contributed work will be credited on the Kaprisoft website
as well as the Creative Realtors website [here.]("https://creativerealtors.net")

### Thank you for your contribution and participation.

###### Malachi Austin, Kaprisoft Core dev team.