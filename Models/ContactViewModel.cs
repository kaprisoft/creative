namespace Creative.Models
{
    public class ContactViewModel
    {
        public string Title { get; set; }
        public string MarthaEmail { get; set; }
        public string JoyceEmail { get; set; }
        public long MarthaPhone { get; set; }
        public long JoycePhone { get; set; }
        public ContactViewModel()
        {
            Title = "";
            MarthaEmail = "marthaehlert@creativerealtors.net";
            JoyceEmail = "joyceaustin@creativerealtors.net";
            MarthaPhone = 6168220721;
            JoycePhone = 6164608020;
        }
    }
}