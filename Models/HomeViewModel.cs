namespace Creative.Models
{
    public class HomeViewModel
    {
        public string Title { get; set; }

        public string SStatus {
            Sold {
                get;
                set;
            }
            Unsold {
                get;
                set;
            }
        }

        public HomeViewModel()
        {
            Title = "";

            SStatus.Sold = "Home has been sold!";
            SStatus.Unsold = "Home is UNSOLD as of @DateTime.NowUtc";
        }
    }
}