using Microsoft.AspNetCore.Mvc;
using Creative.Models;

namespace Creative.Controllers
{
    public class HomeController : Controller
    {
        // GET
        [Route("/")]
        public IActionResult Index()
        {
            ViewData.Model = new HomeViewModel();
            return View();
        }

        [Route("/wip")]
        public IActionResult WorkInProgress()
        {
            ViewData.Model = new HomeViewModel();
            return View();
        }

        [Route("/listings")]
        public IActionResult Listings()
        {
            ViewData.Model = new HomeViewModel();
            return View();
        }
    }
}