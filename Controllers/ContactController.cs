using Creative.Models;
using Microsoft.AspNetCore.Mvc;

namespace Creative.Controllers
{
    public class ContactController : Controller
    {
        [Route("/contact")]
        public IActionResult Index()
        {
            ViewData.Model = new ContactViewModel();
            return View();
        }// Contact information

        [Route("/contact/social-media")]
        public IActionResult Media() 
        {
            ViewData.Model = new ContactViewModel();
            return View();
        } // Social media profiles
    }
}