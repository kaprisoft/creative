using Microsoft.AspNetCore.Mvc;
using Creative.Models;

namespace Creative.Controllers
{
    public class AboutController : Controller
    {
        // GET
        [Route("/about")]
        public IActionResult Index()
        {
            ViewData.Model = new AboutViewModel();
            return View();
        }  // About the Creative Realtors

        [Route("/about/privacy")]
        public IActionResult Privacy()
        {
            ViewData.Model = new AboutViewModel();
            return View();
        } // Privacy Policy

        [Route("/about/release-date")]
        public IActionResult Release()
        {
            ViewData.Model = new AboutViewModel();
            return View();
        }
    }
}